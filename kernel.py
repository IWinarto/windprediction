import numpy as np


def kernel(a, b, width):
    sqdist = np.sum(a**2, 1).reshape(-1, 1) + np.sum(b**2, 1) - 2 * np.dot(a, b.T)
    return np.exp(-.5 / (width**2) * sqdist)
