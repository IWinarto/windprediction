import pandas as pd
import numpy as np


# Require: data is a pandas.DataFrame such that it contains a "date" column where
#          each date is in this format: "YYYYMMDDHH"
# Ensure: each date is converted into the number of hours offset from data.loc[0, 'date'];
#         thus, the first offset is always 0
def convert_timestamps_to_hours(data):
    # convert date column into a string column of timestamps
    data['date'] = data['date'].astype(str)
    train_limit_index = data(data['date'].contains('20101231'))[-1]

    # time series containing the timestamps
    time_series = pd.Series()

    # datetime offset for converting timestamp into hours
    offset = pd.to_datetime(data.loc[0, 'date'][:-2] + " " + data.loc[0, 'date'][-2:])

    # convert timestamp into number of hours from offset
    for idx, time_stamp in data['date'].iteritems():
        date_time = pd.to_datetime(time_stamp[:-2] + " " + time_stamp[-2:])
        date_time = date_time - offset
        time_series.set_value(idx, date_time.days * 24 + date_time.seconds//3600)

    # convert timestamps to number of hours
    data['date'] = time_series
    data = data.rename(columns={'date': 'hour'})

    return data, train_limit_index


# Require: base is a pandas.Timestamp and hours is a positive integer
# Ensure: this function returns base + hours in Datetime
def convert_hours_to_datetime(hours, base):
    return base + pd.Timedelta(seconds=hours*3600)
