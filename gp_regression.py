import numpy as np
import kernel


# Requires: dimensions of x_train and x_test are the same
#           y_train has been standardized
def estimate_mu_cov(x_train, y_train, x_test, param):
    noise_var_est = param[1]
    width = param[0]
    noise_cov = noise_var_est * np.eye(len(x_train))
    stability_matrix = 1e-6 * np.eye(len(x_test))

    # cross-covariance matrix of x_train
    K = kernel(x_train, x_train, width)
    # cross-covariance matrix of x_train and x_test
    K_s = kernel(x_train, x_test, width)
    # cross-covariance matrix of x_test
    K_ss = kernel(x_test, x_test, width)

    # Cholesky decomposition of K added with a tiny amount of constant for stability
    L = np.linalg.cholesky(K + noise_cov)
    # L_k = inv(L) * K_s to simplify calculation and therefore improve efficiency
    v = np.linalg.solve(L, K_s)

    # estimate the mean and cross-covariance
    mu = np.dot(v.T, np.linalg.solve(L, y_train)).reshape((len(x_test),))
    cov = np.linalg.cholesky(K_ss + stability_matrix - np.dot(v.T, v))

    return mu, cov
