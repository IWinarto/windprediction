import pandas as pd
import preprocessing as pp
import numpy as np
import os

processed_data_file = 'processed_train.csv'

if not os.path.isfile(processed_data_file):
    processed_data = pd.read_csv("train.csv")
    processed_data = pp.convert_timestamps_to_hours(processed_data)
    processed_data.to_csv(processed_data_file)
else:
    processed_data = pd.read_csv(processed_data_file)


# for debugging purposes only; comment this out
time_series = processed_data['hour']
listing = [time_series[i + 1] - time_series[i] > 1 for i in np.arange(len(time_series) - 1)]
listing.append(False)
indices = time_series.index[listing]

prev = 13175
counter = 0
for i in indices:
    if counter == 0:
        checkpoint = time_series[i] + 1
        period = 0
    elif counter == 1:
        period = 0
    elif counter == 2:
        counter = 0
        period = time_series[i] + 1 - checkpoint
        checkpoint = time_series[i] + 1

    print("{} {} {} {} {}".format(time_series[i], time_series[i + 1], (time_series[i + 1] - 1) - (time_series[i] + 1)
                                  + 1, time_series[i] - prev + 1, period // 24))
    prev = time_series[i + 1]
    counter += 1

LAG = 10

