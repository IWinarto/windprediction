import numpy as np
import kernel


def slope_of_log(X, y, param, K_y_slope_fun):
    width = param[0]
    noise_var_est = param[1]
    y = y.reshape(-1, 1)

    K_y = kernel.kernel(X, X, width) + noise_var_est * np.eye(len(y))
    K_y_inv = np.linalg.inv(K_y)

    K_y_slope = K_y_slope_fun(width, K_y, len(y))

    a = np.dot(y.T, K_y_inv)
    b = np.dot(K_y_inv, y)

    return (np.dot(a, np.dot(K_y_slope, b)) - np.trace(np.dot(K_y_inv, K_y_slope))) / 2


def with_respect_to_width(*param):
    width = param[0]
    K_y = param[1]
    return (-2 / width) * K_y


def with_respect_to_noise_var(*param):
    len_y = param[2]
    return 2 * np.eye(len_y)
